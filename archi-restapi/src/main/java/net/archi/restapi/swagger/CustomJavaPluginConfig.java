package net.archi.restapi.swagger;

import java.security.Principal;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.property.provider.DefaultModelPropertiesProvider;
import com.mangofactory.swagger.paths.RelativeSwaggerPathProvider;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import com.wordnik.swagger.model.ApiInfo;

@Configuration
@EnableSwagger
@ComponentScan(basePackages = "com.mangofactory.swagger")
public class CustomJavaPluginConfig {

	private SpringSwaggerConfig springSwaggerConfig;

	@Autowired
	DefaultModelPropertiesProvider defaultModelPropertiesProvider;

	@Autowired
	public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
		this.springSwaggerConfig = springSwaggerConfig;
	}

	@Autowired
	ServletContext servletContext;

	@Bean 
	public SwaggerSpringMvcPlugin customImplementation() {
		ObjectMapper objectMapper = new ObjectMapper();
		defaultModelPropertiesProvider.setObjectMapper(objectMapper);

		ArchiAPIPathProvider swaggerPathProvider = new ArchiAPIPathProvider(servletContext);

		return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
				.apiInfo(apiInfo())
				.pathProvider(swaggerPathProvider)
				.includePatterns("/.*")
				.ignoredParameterTypes(HttpServletRequest.class, HttpServletResponse.class, HttpSession.class, Locale.class, Principal.class);
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo(
				"Archi",
				"Archi",
				"Archi",
				"Archi",
				null,
				null
		);
		return apiInfo;
	}

	private class ArchiAPIPathProvider extends RelativeSwaggerPathProvider {

		public ArchiAPIPathProvider(ServletContext servletContext) {
			super(servletContext);
		}

		@Override
		protected String applicationPath() {
			String s = super.applicationPath();
			if (!s.endsWith("/")) {
				s = s + "/";
			}
			return s + "api";
		}
	};
}

