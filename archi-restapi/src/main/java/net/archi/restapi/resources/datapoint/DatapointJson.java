package net.archi.restapi.resources.datapoint;

import net.archi.logic.datapoint.Datapoint;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class DatapointJson {
	
	String label;
	String unit;
	String datapointType;
	String originalDeviceId;
	
	public DatapointJson() {
	}
	
	public DatapointJson(Datapoint dp) {
		label = dp.getLabel();
		originalDeviceId = dp.getOriginalId();
	}
	
	public void copyPropertiesTo(Datapoint dp) {
		dp.setLabel(label);
		dp.setUnit(unit);
	}
	
}
