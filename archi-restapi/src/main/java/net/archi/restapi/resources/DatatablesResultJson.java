package net.archi.restapi.resources;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class DatatablesResultJson {
	
	Object data;
	
	public DatatablesResultJson(Object data){
		this.data = data;
	}
	
}
