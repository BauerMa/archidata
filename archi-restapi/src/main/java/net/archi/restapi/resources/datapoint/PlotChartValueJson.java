package net.archi.restapi.resources.datapoint;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;



@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class PlotChartValueJson {
	
	
		String color;
		
		List<Long[]> data;
		
		String label;
		
		boolean clickable;
		
		boolean hoverable;
		
		float shadowSize;
		
		String highlightColor;
		
		
		public PlotChartValueJson(String color, List<Long[]> data, String label,
				boolean clickable, boolean hoverable,
				float shadowSize, String highlightColor) {
			super();
			this.color = color;
			this.data = data;
			this.label = label;
			this.clickable = clickable;
			this.hoverable = hoverable;
			this.shadowSize = shadowSize;
			this.highlightColor = highlightColor;
		}


}
