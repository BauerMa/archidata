package net.archi.restapi.resources.datapoint;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.Datapoint;
import net.archi.logic.datapoint.DatapointRepository;
import net.archi.restapi.resources.ResultJson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/datapoint")
public class DatapointResources {

	private final Logger LOG = LoggerFactory.getLogger(DatapointResources.class);
	
	@Resource
	private DatapointRepository datapointRepository;
	

	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public List<DatapointJson> listDatapoints() {
		List<Datapoint> datapoints = datapointRepository.findAll();
		List<DatapointJson> list = new ArrayList<>();
		for(Datapoint dp : datapoints){
			list.add(new DatapointJson(dp));
		}
		return list;
	}
	
	@ResponseBody @RequestMapping(value = "/{datapointId}", method = RequestMethod.GET)
	public DatapointJson getDatapoints(@PathVariable Long datapointId) throws Exception {
		Datapoint value = datapointRepository.findById(datapointId);
		return new DatapointJson(value);
	}
	
		
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "/{datapointId}", method = RequestMethod.DELETE)
	public ResultJson deleteValue(@PathVariable Long datapointId) throws Exception {
		Datapoint dp = datapointRepository.findById(datapointId);
		dp.delete();
		LOG.debug("deleted {}", dp);
		return ResultJson.ok();
	}	
	
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.POST)
	public DatapointJson createType(@RequestBody DatapointJson json) throws Exception{
		Datapoint dp = datapointRepository.newInstance();
		json.copyPropertiesTo(dp);
		dp.save();
		LOG.debug("created {}", dp);
		return new DatapointJson(dp);
	}
	
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "/{datapointId}", method = RequestMethod.PUT)
	public DatapointJson updateType(@PathVariable Long datapointId, @RequestBody DatapointJson json) throws Exception {
		Datapoint dp = datapointRepository.findById(datapointId);
		json.copyPropertiesTo(dp);
		dp.save();
		LOG.debug("updated {}", dp);
		return new DatapointJson(dp);
	}
		

}