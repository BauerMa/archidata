package net.archi.restapi.resources;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class ResultJson {
	
	public static enum Status {
		ok, error
	}

	final Status status;
	
	Object data;
	
	String message;
	
	private ResultJson(Status status) {
		this.status = status;
	}
	
	private ResultJson(Status status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public static ResultJson ok() {
		return new ResultJson(Status.ok);
	}

	public static ResultJson error(String message) {
		return new ResultJson(Status.error, message);
	}
	
}
