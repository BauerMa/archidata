package net.archi.restapi.resources.datapoint;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class WeatherSuggResponseJson {
	
	String message;
	boolean needOfChange;
	
	public WeatherSuggResponseJson() {
	}
	
	public WeatherSuggResponseJson(String message, boolean needOfChange) {
		this.message = message;
		this.needOfChange = needOfChange;
	}
	
	
}
