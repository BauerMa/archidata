package net.archi.restapi.resources.user.action;

import net.archi.logic.user.User;
import net.archi.logic.user.action.UserAction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class UserActionJson {
	
	Long id;
	
	String name;
	
	
	public UserActionJson() {
	}
	
	public UserActionJson(UserAction userAction) {
		id = userAction.getId();
		name = userAction.getName();
	}

	public void copyPropertiesTo(UserAction userAction) {
		userAction.setName(name);
	}
	
}
