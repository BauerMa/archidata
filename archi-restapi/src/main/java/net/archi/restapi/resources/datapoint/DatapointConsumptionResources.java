package net.archi.restapi.resources.datapoint;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.logic.datapoint.consumption.DatapointConsumption;
import net.archi.logic.datapoint.consumption.DatapointConsumptionRepository;
import net.archi.logic.user.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/consumption")
public class DatapointConsumptionResources {

	private final Logger LOG = LoggerFactory.getLogger(DatapointConsumptionResources.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	
	@Resource
	private DatapointValueRepository datapointValueRepository;
	@Resource
	private DatapointConsumptionRepository datapointConsumptionRepository;
	@Resource
	private UserRepository userRepository;
	

	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public String getConsumption() throws Exception {
		String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		int persons = userRepository.findAll().size();
		if(persons > 5){
			persons = 5;
		}
		DatapointConsumption consumption =  datapointConsumptionRepository.findByYearAndPersons(year, persons);
		if(consumption==null){
			return null;
		}
		return consumption.getConsumption();
	}
	
	@ResponseBody @RequestMapping(value = "/ownConsumptionToday", method = RequestMethod.GET)
	public String ownConsumption() throws Exception {
		Date morning = sdf.parse(sdf.format(new Date()) + " 00:00");
		Date noon = sdf.parse(sdf.format(new Date()) + " 23:59");
		
		return String.valueOf(datapointValueRepository.findSumOfAll(morning, noon));
	}
	
	@ResponseBody @RequestMapping(value = "/ownConsumptionWeek", method = RequestMethod.GET)
	public String ownConsumptinoWeek() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(sdf.format(new Date()) + " 00:00"));
		c.add(Calendar.DAY_OF_WEEK, -7);
		
		Date noon = sdf.parse(sdf.format(new Date()) + " 23:59");
		
		return String.valueOf(datapointValueRepository.findSumOfAll(c.getTime(), noon));
	}
	
	@ResponseBody @RequestMapping(value = "/ownConsumptionMonth", method = RequestMethod.GET)
	public String ownConsumptinoMonth() throws Exception {
		LocalDate monthBegin = LocalDate.ofEpochDay(System.currentTimeMillis() / (24 * 60 * 60 * 1000) ).withDayOfMonth(1);
		Instant instant = monthBegin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		
		Date morning = Date.from(instant);
		Date noon = sdf.parse(sdf.format(new Date()) + " 23:59");
		
		return String.valueOf(datapointValueRepository.findSumOfAll(morning, noon));
	}

}