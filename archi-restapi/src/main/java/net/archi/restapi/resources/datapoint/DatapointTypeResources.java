package net.archi.restapi.resources.datapoint;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.restapi.resources.DatatablesResultJson;
import net.archi.restapi.resources.ResultJson;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/types")
public class DatapointTypeResources {

	private final Logger LOG = Logger.getLogger(DatapointTypeResources.class);
	
	@Resource
	private DatapointTypeRepository datapointTypeRepository;
	

	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public List<DatapointTypeJson> listTypes() {
		List<DatapointType> datapointTypes = datapointTypeRepository.findAll(); 
		
		List<DatapointTypeJson> list = new ArrayList<>();
		for(DatapointType dp : datapointTypes){
			list.add(new DatapointTypeJson(dp));
		}
		return list;
	}
	
	@ResponseBody @RequestMapping(value = "/{typeId}", method = RequestMethod.GET)
	public DatapointTypeJson getType(@PathVariable Long typeId) throws Exception {
		DatapointType type = datapointTypeRepository.findById(typeId);
		return new DatapointTypeJson(type);
	}
	
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.POST)
	public DatatablesResultJson createType(HttpServletRequest request) throws Exception{
		DatapointType type = datapointTypeRepository.newInstance();

		String name = request.getParameter("data[0][name]");
		String tattern = request.getParameter("data[0][pattern]");
		String pType = request.getParameter("data[0][patterntype]");
		String maxVal = request.getParameter("data[0][maxValue]");
		String minVal = request.getParameter("data[0][minValue]");
	
		type.setName(name);
		type.setPattern(tattern);
		type.setPatterntype(pType);
		type.setMaxValue(Double.valueOf(maxVal));
		type.setMinValue(Double.valueOf(minVal));
		type.save();
		LOG.debug("created " +  type);
		return new DatatablesResultJson(listTypes());
	}
	
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.PUT)
	public DatatablesResultJson updateType(HttpServletRequest request) throws Exception {
		String id = request.getParameter("data[undefined][id]");
		String name = request.getParameter("data[undefined][name]");
		String tattern = request.getParameter("data[undefined][pattern]");
		String maxVal = request.getParameter("data[undefined][maxValue]");
		String minVal = request.getParameter("data[undefined][minValue]");
		
		DatapointType type = datapointTypeRepository.findById(Long.valueOf(id));
		type.setName(name);
		type.setPattern(tattern);
		type.setPatterntype(tattern);
		type.setMaxValue(Double.valueOf(maxVal));
		type.setMinValue(Double.valueOf(minVal));
		
		type.save();
		LOG.debug("updated "  + type);
		return new DatatablesResultJson(listTypes());
	}
		
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.DELETE)
	public ResultJson deleteType(HttpServletRequest request) throws Exception {
		String id = request.getParameter("data[undefined][id]");
		DatapointType type = datapointTypeRepository.findById(Long.valueOf(id));
		type.delete();
		LOG.debug("deleted " + type);
		return ResultJson.ok();
	}	

}
