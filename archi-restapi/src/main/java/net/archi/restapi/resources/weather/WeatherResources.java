package net.archi.restapi.resources.weather;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.archi.logic.weather.Weather;
import net.archi.logic.weather.WeatherRepository;
import net.archi.restapi.resources.DatatablesResultJson;
import net.archi.restapi.resources.datapoint.PlotChartValueJson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WeatherResources {

	private final Logger LOG = LoggerFactory.getLogger(WeatherResources.class);
	
	@Resource
	WeatherRepository weatherRepository;
	
	
	@ResponseBody @RequestMapping(value = "/weather", method = RequestMethod.GET)
	public List<WeatherJson> listAllW() {
		List<Weather> ww = weatherRepository.findAll();
		List<WeatherJson> list = new ArrayList<>();
		for(Weather dp : ww){
			if(!list.contains(dp)){
				list.add(new WeatherJson(dp));
			}
		}
		return list;
	}
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "/weather/{weatherId}", method = RequestMethod.GET)
	public WeatherJson getW(@PathVariable Long weatherId) throws Exception {
		Weather ww = weatherRepository.findById(weatherId);
		return new WeatherJson(ww);
	}
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "/weather", method = RequestMethod.POST)
	public DatatablesResultJson createW(HttpServletRequest request) throws Exception {
		
		String temperature = request.getParameter("data[0][temperature]");
		String type = request.getParameter("data[0][type]");
		String sun = request.getParameter("data[0][sun]");
		String wind = request.getParameter("data[0][windSpeed]");
		Weather ww = weatherRepository.newInstance();
		ww.setName(temperature);
		ww.setTemperature(temperature);
		ww.setType(type);
		ww.setSun(sun);
		ww.setWindSpeed(wind);
		ww.save();
		LOG.debug("new weather");
		
		return new DatatablesResultJson(listAllW());
	}
	
	@ResponseBody @RequestMapping(value = "/weather", method = RequestMethod.PUT)
	public DatatablesResultJson saveW(HttpServletRequest request) throws Exception {
		String id = request.getParameter("data[undefined][id]");
		String temperature = request.getParameter("data[undefined][temperature]");
		String type = request.getParameter("data[undefined][type]");
		String sun = request.getParameter("data[undefined][sun]");
		String wind = request.getParameter("data[undefined][windSpeed]");
		Weather ww = weatherRepository.findById(Long.valueOf(id));
		ww.setName(temperature);
		ww.setTemperature(temperature);
		ww.setType(type);
		ww.setSun(sun);
		ww.setWindSpeed(wind);
		ww.save();

		return new DatatablesResultJson(listAllW());
	}
	
	
	@ResponseBody @RequestMapping(value = "/weatherLast7D", method = RequestMethod.GET)
	public PlotChartValueJson listLastWeathers() {
		List<Weather> ww = weatherRepository.findLast7();
		List<Long[]> data = new ArrayList<>();
		for(Weather myW : ww){
			String temp = myW.getTemperature().substring(0, 3);
			Double doubleTemp = Double.valueOf(temp);
			data.add(new Long[]{myW.getDateOfWeather().getTime(),  doubleTemp.longValue()});
		}
		
		PlotChartValueJson line = new PlotChartValueJson("blue", data, "Wetter", false, false, 0f, "");
		return line;
	}


}
