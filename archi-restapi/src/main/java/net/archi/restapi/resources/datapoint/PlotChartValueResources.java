package net.archi.restapi.resources.datapoint;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/plotchart")
public class PlotChartValueResources {

	private final Logger LOG = LoggerFactory.getLogger(PlotChartValueResources.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

	
	@Resource
	private DatapointValueRepository datapointValueRepository;
	@Resource
	private DatapointTypeRepository datapointTypeRepository;
	
	String[] colors = new String[]{"blue", "yellow", "red"};
	
	

	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public PlotChartValueJson[] listValues(@RequestParam("typeFilter") String type) throws Exception {
		List<PlotChartValueJson> plotChartValueJson = new ArrayList<PlotChartValueJson>();
		
		Date from =null;
		Date to = null;
		
		if(type.equals("day")){
			from = sdf.parse(sdf.format(new Date()) + " 00:00");
			to = sdf.parse(sdf.format(new Date()) + " 23:59");
		}else if(type.equals("week")){
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(sdf.format(new Date()) + " 00:00"));
			c.add(Calendar.DAY_OF_WEEK, -7);
			from = c.getTime();
			
			to = sdf.parse(sdf.format(new Date()) + " 23:59");
		} else if (type.equals("month")){
			LocalDate monthBegin = LocalDate.ofEpochDay(System.currentTimeMillis() / (24 * 60 * 60 * 1000) ).withDayOfMonth(1);
			Instant instant = monthBegin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
			
			from = Date.from(instant);
			to = sdf.parse(sdf.format(new Date()) + " 23:59");
		}
		
	
		List<DatapointType> types = datapointTypeRepository.findAll();
		
		for(DatapointType t : types){
				List<DatapointValue> datapointvalues = datapointValueRepository.findValueInTimeAreaForType(t.getId(),from, to); 

				if(datapointvalues == null || datapointvalues.size() <1){
					LOG.info("no datapointvalues for type with name " +  t.getName());
					continue;
				}
				
				List<Long[]> data = new ArrayList<>();
				for(DatapointValue val : datapointvalues){
					data.add(new Long[]{val.getValueDate().getTime(), (val.getValue()).longValue()});
				}
				
				int i = types.indexOf(t);
				String color = colors[i % colors.length];
				
				PlotChartValueJson chartVal = new PlotChartValueJson(color, data,  t.getName(),
						true, true, 1f, color);
				plotChartValueJson.add(chartVal);
		}
		
		
		return plotChartValueJson.toArray(new PlotChartValueJson[]{});
	}
	

}