package net.archi.restapi.resources.datapoint;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.Datapoint;
import net.archi.logic.datapoint.DatapointRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.restapi.resources.ResultJson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/datapoint/{datapointId}/values")
public class DatapointValueResources {

	private final Logger LOG = LoggerFactory.getLogger(DatapointValueResources.class);
	
	@Resource
	private DatapointValueRepository datapointValueRepository;
	@Resource
	private DatapointRepository datapointRepository;
	

	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public List<DatapointValueJson> listValues(@PathVariable Long datapointId) {
		List<DatapointValueJson> list = new ArrayList<>();
		Datapoint datapoint;
		try {
			datapoint = datapointRepository.findById(datapointId);
			List<DatapointValue> datapointvalues = datapointValueRepository.getLastValues(datapoint, 20); 
			for(DatapointValue dp : datapointvalues){
				list.add(new DatapointValueJson(dp));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	@ResponseBody @RequestMapping(value = "/{valueId}", method = RequestMethod.GET)
	public DatapointValueJson getValue(@PathVariable Long datapointId, @PathVariable Long valueId) throws Exception {
		DatapointValue value = datapointValueRepository.findById(valueId);
		return new DatapointValueJson(value);
	}
	
		
	@Transactional(readOnly = false)
	@ResponseBody @RequestMapping(value = "/{valueId}", method = RequestMethod.DELETE)
	public ResultJson deleteValue(@PathVariable Long datapointId, @PathVariable Long valueId) throws Exception {
		DatapointValue value = datapointValueRepository.findById(valueId);
		value.delete();
		LOG.debug("deleted {}", value);
		return ResultJson.ok();
	}

}