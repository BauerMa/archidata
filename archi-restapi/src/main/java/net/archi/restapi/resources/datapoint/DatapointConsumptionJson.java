package net.archi.restapi.resources.datapoint;

import net.archi.logic.datapoint.consumption.DatapointConsumption;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class DatapointConsumptionJson {
	
	String name;
	String consumption;
	String year;
	int persons;
	
	public DatapointConsumptionJson() {
	}
	
	public DatapointConsumptionJson(DatapointConsumption dp) {
		name = dp.getName();
		consumption = dp.getConsumption();
		persons = dp.getPersons();
		year = dp.getYear();
				
		
	}
	
}
