package net.archi.restapi.resources.weather;

import java.util.Date;

import net.archi.logic.weather.Weather;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class WeatherJson {
	
	Long id;
	
	String temperature;
	
	String windSpeed;
	
	String sun;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm")
	Date dateOfWeather;
	
	String type;
	
	
	public WeatherJson() {
	}
	
	public WeatherJson(Weather weather) {
		id = weather.getId();
		type = weather.getType();
		temperature = weather.getTemperature();
		windSpeed = weather.getWindSpeed();
		sun = weather.getSun();
		dateOfWeather = weather.getDateOfWeather();
	}

	
}
