package net.archi.restapi.resources.user;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.archi.logic.user.User;
import net.archi.logic.user.UserRepository;
import net.archi.restapi.resources.DatatablesResultJson;
import net.archi.restapi.resources.ResultJson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/user")
public class UserResources {

	private final Logger LOG = LoggerFactory.getLogger(UserResources.class);
	
	@Resource
	UserRepository userRepository;
	
	
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.GET)
	public List<UserJson> listUsers() {
		
		List<User> users = userRepository.findAll();
		List<UserJson> list = new ArrayList<>();
		for(User dp : users){
			list.add(new UserJson(dp));
		}
		return list;
	}
	
	@ResponseBody @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public UserJson getUser(@PathVariable Long userId) throws Exception {
		User user = userRepository.findById(userId);
		return new UserJson(user);
	}
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.POST)
	public DatatablesResultJson createUser(HttpServletRequest request) {
		
		String name = request.getParameter("data[0][name]");
		String lastName = request.getParameter("data[0][lastName]");
		
		User user = userRepository.newInstance();
		user.setName(name);
		user.setLastName(lastName);
		user.save();
		LOG.debug("created {}", user);
		return new DatatablesResultJson(listUsers());
	}
	
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.PUT)
	public DatatablesResultJson updateUser(HttpServletRequest request) throws Exception {
		String id = request.getParameter("data[undefined][id]");
		String name = request.getParameter("data[undefined][name]");
		String lastName = request.getParameter("data[undefined][lastName]");
		
		User u = userRepository.findById(Long.valueOf(id));
		u.setName(name);
		u.setLastName(lastName);
		u.save();
		return new DatatablesResultJson(listUsers());
	}
	

	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "", method = RequestMethod.DELETE)
	public ResultJson deleteUser(HttpServletRequest request) throws Exception {
		String id = request.getParameter("data[undefined][id]");
		User user = userRepository.findById(Long.valueOf(id));
		user.delete();
		LOG.debug("deleted {}", user);			
		return ResultJson.ok();
	}
	
}
