package net.archi.restapi.resources.datapoint;

import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.Datapoint;
import net.archi.logic.datapoint.DatapointRepository;
import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.logic.datapoint.consumption.DatapointConsumptionRepository;
import net.archi.logic.user.UserRepository;
import net.archi.logic.weather.Weather;
import net.archi.logic.weather.WeatherRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/infos")
public class CombinedInformationResources {

	private final Logger LOG = LoggerFactory.getLogger(CombinedInformationResources.class);
	
	@Resource	private DatapointRepository datapointRepository;
	@Resource	private DatapointTypeRepository datapointTypeRepository;
	@Resource	private DatapointConsumptionRepository datapointConsumptionRepository;
	@Resource	private UserRepository userRepository;
	@Resource	private WeatherRepository weatherRepository;
	@Resource	private DatapointValueRepository datapointValueRepository;
	
	
	double temperatureSugg = 21;
	
	@ResponseBody @RequestMapping(value = "/temperatureSugg", method = RequestMethod.GET)
	public WeatherSuggResponseJson getConsumption() throws Exception {
		String response = "";
		Weather w = weatherRepository.findAcutalWeather();
		Weather w_pred = weatherRepository.findLatestWeatherPrediction();
		DatapointType doortype = datapointTypeRepository.findByName("door");
		if(doortype == null){
			return null;
		}
		List<Datapoint> doors =  datapointRepository.findByOrignialId("Item5");
		boolean needOfChange = false;
		
		if(Double.valueOf(w.getTemperature()) > temperatureSugg){
			response = "Mit " + w.getTemperature() + " Grad C Außentemperatur ist es draußen wärmer als empfohlene Raumtemperatur";
		}
		int openDoors = this.countOfOpenDoors(doors);
		if(this.countOfOpenDoors(doors) > 1){
			response += "\n Anzahl offener Türen und Fenster ist " + openDoors;
		} else {
			response += "\n Türen und Fenster sind alle geschlossen";
		}
		if(openDoors != 0 && Double.valueOf(w.getTemperature()) < temperatureSugg ){
			response += "\n Es ist draußen mit " + w.getTemperature() + " Grad C kälter als empfohlen, Türen und Fenster schließen!";
			needOfChange = true;
			
		} else if(openDoors != 0 && Double.valueOf(w_pred.getTemperature()) < temperatureSugg ){
			response += "\n Wetterbericht sagt für morgen " + w_pred.getTemperature() + " vorher, Türen und Fenster schließen!";
			needOfChange = true;
		}
		
		return new WeatherSuggResponseJson(response, needOfChange);
	}
	

	private int countOfOpenDoors(List<Datapoint> doors){
		int count = 0;
		
		for(Datapoint dp : doors){
			List<DatapointValue> lastval = datapointValueRepository.getLastValues(dp, 1);
			if(lastval.size() > 0 && lastval.get(0).getValue() > 0.0){
				count +=1;
			}
			
		}
		return count;
	}
}