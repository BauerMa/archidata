package net.archi.restapi.resources.datapoint;

import java.util.Date;

import net.archi.logic.datapoint.DatapointValue;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class DatapointValueJson {
	
	Double value;
	Date valueDate;
	
	public DatapointValueJson() {
	}
	
	public DatapointValueJson(DatapointValue dpValue) {
		value = dpValue.getValue();
		valueDate = dpValue.getValueDate();
	}
	
}
