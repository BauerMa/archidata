package net.archi.restapi.resources.datapoint;

import net.archi.logic.datapoint.DatapointType;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class DatapointTypeJson {
	
	Long id;
	String name;
	String patterntype;
	String pattern;
	Double maxValue;
	Double minValue;
	
	
	public DatapointTypeJson() {
	}
	
	public DatapointTypeJson(DatapointType type) {
		id = type.getId();
		patterntype = type.getPatterntype();
		pattern = type.getPattern();
		name = type.getName();
		maxValue = type.getMaxValue();
		minValue = type.getMinValue();
	}

	public void copyPropertiesTo(DatapointType type) {
		type.setName(name);
		type.setPatterntype(patterntype);
		type.setPattern(pattern);
		type.setMaxValue(maxValue);
		type.setMinValue(minValue);
	}
	
}
