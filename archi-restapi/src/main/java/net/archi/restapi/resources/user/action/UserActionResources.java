package net.archi.restapi.resources.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.user.action.UserAction;
import net.archi.logic.user.action.UserActionRepository;
import net.archi.restapi.resources.ResultJson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/userAction")
public class UserActionResources {

	private final Logger LOG = LoggerFactory.getLogger(UserActionResources.class);
	
	@Resource
	UserActionRepository userActionRepository;
	
	
	@ResponseBody @RequestMapping(value = "/", method = RequestMethod.GET)
	public List<UserActionJson> listUsersAction() {
		
		List<UserAction> usersAction = userActionRepository.findAll();
		List<UserActionJson> list = new ArrayList<>();
		for(UserAction dp : usersAction){
			list.add(new UserActionJson(dp));
		}
		return list;
	}
	
	@ResponseBody @RequestMapping(value = "/usersAction/{userActionId}", method = RequestMethod.GET)
	public UserActionJson getUser(@PathVariable Long userActionId) throws Exception {
		UserAction user = userActionRepository.findById(userActionId);
		return new UserActionJson(user);
	}
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "/usersAction", method = RequestMethod.POST)
	public UserActionJson createUser( @RequestBody UserActionJson json) {
		UserAction userAction = userActionRepository.newInstance();
		json.copyPropertiesTo(userAction);
		userAction.save();
		LOG.debug("created {}", userAction);
		return new UserActionJson(userAction);
	}
	
	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "/usersAction/{userActionId}", method = RequestMethod.PUT)
	public UserActionJson updateUser(@PathVariable Long userActionId, @RequestBody UserActionJson json) throws Exception {
		UserAction userAction = userActionRepository.findById(userActionId);
		json.copyPropertiesTo(userAction);
		userAction.save();
		LOG.debug("updated {}", userAction);
		return new UserActionJson(userAction);
	}

	@Transactional(readOnly=false)
	@ResponseBody @RequestMapping(value = "/users/{userActionId}", method = RequestMethod.DELETE)
	public ResultJson deleteUserAction(@PathVariable Long userActionId) throws Exception {
		UserAction userAction = userActionRepository.findById(userActionId);
		userAction.delete();
		LOG.debug("deleted {}", userAction);			
		return ResultJson.ok();
	}
	
}
