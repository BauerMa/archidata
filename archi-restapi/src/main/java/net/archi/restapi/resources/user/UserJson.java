package net.archi.restapi.resources.user;

import net.archi.logic.user.User;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class UserJson {
	
	Long id;
	
	String name;
	
	String lastName;
	
	
	public UserJson() {
	}
	
	public UserJson(User user) {
		id = user.getId();
	
		name = user.getName();
		lastName = user.getLastName();
	}

	public void copyPropertiesTo(User user) {
		user.setName(name);
		user.setLastName(lastName);
	}
	
}
