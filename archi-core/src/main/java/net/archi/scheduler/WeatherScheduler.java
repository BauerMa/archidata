package net.archi.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import net.archi.logic.user.UserRepository;
import net.archi.logic.weather.Weather;
import net.archi.logic.weather.WeatherRepository;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class WeatherScheduler{
	
	private final Logger LOG = Logger.getLogger(WeatherScheduler.class);

	@Resource
	private UserRepository userRepository;
	@Resource
	private WeatherRepository weatherRepository;
	
	String latitude ="48.135125";
	String longitude ="11.581981";
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Scheduled(cron="0 0 0/3 * * ?")
	public void getWeatherFromTodayAndTomorrow(){
		LOG.debug("running weather update");
		
			String forecastURL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=" + latitude + "&lon=" + longitude + "&cnt=1&mode=json";
			String actualURL = "http://api.openweathermap.org/data/2.5/weather?lat=" + latitude +  "&lon=" + longitude;
			
			String forecast = this.getWeatherForLocationAndType(forecastURL);
			String actual = this.getWeatherForLocationAndType(actualURL);
			
			Weather forecastBean = this.generateWeatherObjectFromStringTomorrow(forecast);
			Weather actualBean = this.generateWeatherObjectFromStringToday(actual);
			
			forecastBean.save();
			actualBean.save();
		
		
		LOG.debug(" weather update done");
	}
	
	private Weather generateWeatherObjectFromStringTomorrow(String result){
		JSONObject jsonfirst = new JSONObject(result);

		JSONObject tomorrow = ((JSONObject)jsonfirst.getJSONArray("list").get(0));
		Double temperatureFahrenheit = tomorrow.getJSONObject("temp").getDouble("day");
		Double windSpeed = tomorrow.getDouble("speed");
		Double sun = tomorrow.getDouble("clouds");
		Double temperature = temperatureFahrenheit - 273.15;
		
		Weather weather = weatherRepository.newInstance();
		weather.setType("actual");
		Date tomo = new Date(new Date().getTime() + (1000 * 60 * 60 * 24));
		weather.setName("actual-" + sdf.format(tomo));
		weather.setDateOfWeather(tomo);
		weather.setTemperature(String.valueOf(temperature));
		weather.setWindSpeed(String.valueOf(windSpeed));
		weather.setSun(String.valueOf(sun));
		
		return weather;
	}
	
	private Weather generateWeatherObjectFromStringToday(String result){
		JSONObject jsonfirst = new JSONObject(result);

		JSONObject today = (JSONObject)jsonfirst;
		Double temperatureFahrenheit = today.getJSONObject("main").getDouble("temp");
		Double windSpeed = today.getJSONObject("wind").getDouble("speed");
		Double sun =  today.getJSONObject("clouds").getDouble("all");
		Double temperature = temperatureFahrenheit - 273.15;
		
		Weather weather = weatherRepository.newInstance();
		weather.setType("forecast");
		weather.setDateOfWeather(new Date());
		weather.setName("actual-" + sdf.format(new Date()));
		weather.setTemperature(String.valueOf(temperature));
		weather.setWindSpeed(String.valueOf(windSpeed));
		weather.setSun(String.valueOf(sun));
		
		return weather;
	}
	
	
	private String getWeatherForLocationAndType(String urlForWeather){
		URL url;
	    HttpURLConnection conn;
	    BufferedReader rd;
	    String line;  
	      String result = "";
	    try {
	       url = new URL(urlForWeather);
	       conn = (HttpURLConnection) url.openConnection();
	       conn.setRequestMethod("GET");
	       rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	       while ((line = rd.readLine()) != null) {
	          result += line;
	       }
	       rd.close();
	    } catch (IOException e) {
	       e.printStackTrace();
	    } catch (Exception e) {
	       e.printStackTrace();
	    }
		
		return result;
	}
	

}
