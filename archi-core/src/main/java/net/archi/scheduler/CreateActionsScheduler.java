package net.archi.scheduler;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.Datapoint;
import net.archi.logic.datapoint.DatapointAction;
import net.archi.logic.datapoint.DatapointActionRepository;
import net.archi.logic.datapoint.DatapointRepository;
import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.logic.datapointdecision.TypeDecider;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class CreateActionsScheduler {
	
	private final Logger LOG = Logger.getLogger(CreateActionsScheduler.class);

	@Resource	DatapointRepository datapointRepository;
	@Resource	DatapointValueRepository datapointValueRepository;
	@Resource	DatapointActionRepository datapointActionRepository;
	@Resource	DatapointTypeRepository datapointTypeRepository;
	@Resource	TypeDecider typeDecider;
	
	@Scheduled(cron="30 * * * * ?") //every minute
	public void createActions(){
		LOG.debug("start generating actions");
		List<Datapoint> allDPs = datapointRepository.findAll();
		
		for(Datapoint dp: allDPs){
			List<DatapointValue> values = datapointValueRepository.findAllByDatapoint(dp.getId());
			if(values.size() <1){
				LOG.debug("no values of dp");
				continue;
			}
			
			DatapointValue firstnull11 = this.getFirstNullVal(values);
			if(firstnull11 == null){
				LOG.debug("no null val found, create action from all");
				DatapointAction thisA = datapointActionRepository.newInstance();
				thisA.setName(values.get(0).getName());
				thisA.save();
				
				DatapointType type = typeDecider.getDatapointTypeForDatapointVals(values);
				if(type != null){
					thisA.setDatapointType(type);
					thisA.save();
				}
				
				for(DatapointValue vall : values){
					vall.setDatapointActionBean(thisA);
					vall.setDatapointBean(null);
					if(type != null){
						vall.setDatapointTypeId(type.getId());
					}
					vall.save();
				}
				
				
				continue;
			}
			
			while(values != null && values.size() > 0 && !allNullVals(values)){
				int i = 0;
				while(isNullVal(values.get(i))){
					i++;
				}
				
				List<DatapointValue> subsequence = this.getValuesTillNull(values.subList(i, values.size()));
				List<DatapointValue> nullVals = values.subList(0, i);
				subsequence.addAll(nullVals);
				
				if(subsequence.size() < 1){
					continue;
				}
				
				DatapointAction thisA = datapointActionRepository.newInstance();
				thisA.setName(subsequence.get(0).getName());
				thisA.save();
				
				DatapointType type = typeDecider.getDatapointTypeForDatapointVals(subsequence);
				if(type != null){
					thisA.setDatapointType(type);
					thisA.save();
				}
				for(DatapointValue vall : subsequence){
					vall.setDatapointActionBean(thisA);
					vall.setDatapointBean(null);
					if(type!= null){
						vall.setDatapointTypeId(type.getId());
					}
					vall.save();
				}
				
				
				if(subsequence.size() == values.size()){
					values = null;
				}else{
					values = values.subList(subsequence.size() +1 , values.size());
				}
			}
		}
		LOG.debug("done generating actions");
		
		
	}
	
	private boolean allNullVals(List<DatapointValue> vals){
		for(DatapointValue v : vals){
			if(v.getValue()> 10.0){
				return false;
			}
		}
		return true;
	}

	private DatapointValue getFirstNullVal(List<DatapointValue> values){
		for(DatapointValue v : values){
			if(v.getValue()< 10.0){
				return v;
			}
		}
		return null;
	}
	
	private List<DatapointValue> getValuesTillNull(List<DatapointValue> values){
		List<DatapointValue> vals = new ArrayList<>();
		for(DatapointValue v : values){
			vals.add(v);
			if(v.getValue()< 10.0){
				return vals;
			}
		}
		return vals;
	}
	
	private boolean isNullVal(DatapointValue v){
		if(v.getValue()< 10.0){
			return true;
		}
		return false;
	}
	
			
	
}
