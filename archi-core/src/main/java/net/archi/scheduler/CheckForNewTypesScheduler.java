package net.archi.scheduler;

import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointAction;
import net.archi.logic.datapoint.DatapointActionRepository;
import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.logic.datapointdecision.TypeDecider;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CheckForNewTypesScheduler {

	private final Logger LOG = Logger.getLogger(CheckForNewTypesScheduler.class);

	@Resource	DatapointActionRepository datapointActionRepository;
	@Resource	DatapointTypeRepository datapointTypeRepository;
	@Resource	DatapointValueRepository datapointValueRepository;
	@Resource	TypeDecider typeDecider;

	@Scheduled(cron = "0 * * * * ?")
	public void checkForNewTypes() {
		List<DatapointAction> allDPs = datapointActionRepository.findAllWithoutType();
		LOG.debug(allDPs.size() + " actions to fill");

		for (DatapointAction act : allDPs) {
			List<DatapointValue> values = datapointValueRepository.findAllByAction(act.getId());

			DatapointType type = typeDecider.getDatapointTypeForDatapointVals(values);
			if (type != null) {
				act.setDatapointType(type);
				for(DatapointValue vall : datapointValueRepository.findAllByAction(act.getId())){
					vall.setDatapointTypeId(type.getId());
					vall.save();
				}
				act.save();
			}
		}
		LOG.debug("done finding types");
		
	}

}
