package net.archi.scheduler;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.Datapoint;
import net.archi.logic.datapoint.DatapointRepository;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;
import net.archi.logic.datapointdecision.TypeDecider;
import net.archi.logic.externalDB.ExternalDBValueBean;
import net.archi.logic.externalDB.ValueDBConnector;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class NewDPValuesScheduler {
	
	private final Logger LOG = Logger.getLogger(NewDPValuesScheduler.class);

	@Resource
	DatapointValueRepository datapointValueRepository;
	
	@Resource
	DatapointRepository datapointRepository;
	
	@Resource
	DatapointTypeRepository datapointTypeRepository;


	@Resource
	ValueDBConnector valueDBConnector;
	
	@Resource
	TypeDecider typeDecider;
	
	@Scheduled(cron="0 * * * * ?") //every minute
	public void fetchNewDPValuesFromExternalDP(){
		
		HashMap< String, String> tableNames = valueDBConnector.generateTableNames();
		
		try {
			if(importOldValues()){
				List<ExternalDBValueBean> values = valueDBConnector.generateValueSQLQuery(null, null);
				generateDatpointValueFromExternalVal(values, tableNames);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int count = 0;
		Date to = new Date();
		Date from = new Date(to.getTime() - 1 * 60 * 1000);
			try {
				List<ExternalDBValueBean> values = valueDBConnector.generateValueSQLQuery(from, to);
				count = values.size();
				generateDatpointValueFromExternalVal(values, tableNames);	
			} catch (SQLException e) {
				e.printStackTrace();
			}
		LOG.debug("fetched " + count + " values from DB for "+ from + " to " + to);
	}
	
	
	private boolean importOldValues(){
		return datapointValueRepository.findAll().size() < 3;		
	}
	
	private void generateDatpointValueFromExternalVal(List<ExternalDBValueBean> externVals, HashMap<String, String> tableNames){
		List<Datapoint> allDps = datapointRepository.findAll();
		
		HashMap<String, Datapoint> valuesPerTable = new HashMap<String, Datapoint>();
		for(Datapoint dp : allDps){
			valuesPerTable.put(dp.getOriginalId(), dp);
		}
		
		
		for(ExternalDBValueBean val : externVals){
			DatapointValue dvp = datapointValueRepository.newInstance();
			
			dvp.setName(tableNames.get(val.getTableSource()));
			dvp.setValue(val.getValue());
			dvp.setValueDate(val.getTimestamp());
			dvp.setOriginalId(val.getTableSource());
			
			Datapoint dp = valuesPerTable.get(val.getTableSource());
			if(dp == null){
				dp = datapointRepository.newInstance();
				dp.setName(val.getTableSource());
				dp.setOriginalId(val.getTableSource());
				dp.save();
				valuesPerTable.put(val.getTableSource(), dp);
			}
			
			dvp.setDatapointBean(dp);
			dvp.save();
		}
		
	}
			
	
}
