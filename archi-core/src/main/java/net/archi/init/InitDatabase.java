package net.archi.init;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.consumption.DatapointConsumption;
import net.archi.logic.datapoint.consumption.DatapointConsumptionRepository;
import net.archi.logic.user.User;
import net.archi.logic.user.UserRepository;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class InitDatabase {
	
	private final Logger LOG = Logger.getLogger(InitDatabase.class);
	
	@Resource DatapointTypeRepository datapointTypeRepository;
	@Resource UserRepository userRepository;
	@Resource DatapointConsumptionRepository datapointConsumptionRepository;
	
	
	@PostConstruct
	public void init(){
		LOG.debug("---\n---\n--- started init ");
		List< DatapointType> types = datapointTypeRepository.findAll();
		
		if(!containsInitType(types)){ //init types
			DatapointType typeDoor = datapointTypeRepository.newInstance();
			typeDoor.setName("door");
			typeDoor.setPatterntype("door");
			typeDoor.save();
			
			DatapointType typePC = datapointTypeRepository.newInstance();
			typePC.setName("PC");
			typePC.setPatterntype("constant");
			typePC.setPattern("100;50;10");
			typePC.setMaxValue(100D);
			typePC.setMinValue(0D);
			typePC.save();
			
			DatapointType typeTV = datapointTypeRepository.newInstance();
			typeTV.setName("TV");
			typeTV.setPatterntype("constant");
			typeTV.setPattern("40;50;10");
			typeTV.setMaxValue(100D);
			typeTV.setMinValue(0D);
			typeTV.save();
			
			DatapointType typeWasch = datapointTypeRepository.newInstance();
			typeWasch.setName("Waschmaschine");
			typeWasch.setPatterntype("constant");
			typeWasch.setPattern("190;10;10");
			typeWasch.setMaxValue(250D);
			typeWasch.setMinValue(0D);
			typeWasch.save();
			
			DatapointType typK = datapointTypeRepository.newInstance();
			typK.setName("Wasserkocher");
			typK.setPatterntype("constant");
			typK.setPattern("2000;100;50");
			typK.setMaxValue(2500D);
			typK.setMinValue(0D);
			typK.save();
		}
		
		List<User> user = userRepository.findAll();
		
		if(user.size() <1){ //init types
			User myU = userRepository.newInstance();
			myU.setName("Max");
			myU.setLastName("Mustermann");
			myU.save();
		}
		
		
		//consumption
		if(datapointConsumptionRepository.findAll().size() <1){
			DatapointConsumption cons = datapointConsumptionRepository.newInstance();
			cons.setConsumption("2700000");
			cons.setName("2015-1");
			cons.setPersons(1);
			cons.setYear("2015");
			cons.save();
			
			DatapointConsumption cons1 = datapointConsumptionRepository.newInstance();
			cons1.setConsumption("3200000");
			cons1.setName("2015-2");
			cons1.setPersons(2);
			cons1.setYear("2015");
			cons1.save();
			
			
			DatapointConsumption cons2 = datapointConsumptionRepository.newInstance();
			cons2.setConsumption("4000000");
			cons2.setName("2015-3");
			cons2.setPersons(3);
			cons2.setYear("2015");
			cons2.save();
			
			
			DatapointConsumption cons3 = datapointConsumptionRepository.newInstance();
			cons3.setConsumption("4400000");
			cons3.setName("2015-4");
			cons3.setPersons(4);
			cons3.setYear("2015");
			cons3.save();
			
			
			DatapointConsumption cons4 = datapointConsumptionRepository.newInstance();
			cons4.setConsumption("5500000");
			cons4.setName("2015-5");
			cons4.setPersons(5);
			cons4.setYear("2015");
			cons4.save();
		}
		
		LOG.info("\n---\n---");
	}
	
	
	private boolean containsInitType(List< DatapointType> types){
		for(DatapointType type : types){
			if(type.getName().equals("door")){
				return true;
			}
		}
		return false;
	}

}
