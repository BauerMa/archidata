package net.archi.logic.datapointdecision;

import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointValue;

import org.springframework.stereotype.Component;

@Component
public class FunctionTypeDecision {
	
	
	public boolean isThisTypeValid(DatapointType type, List<DatapointValue> values){
		String[] pattern = type.getPattern().split(";"); //pattern: funct, tolerance
		String function = pattern[0];
		Double tolerance = Double.valueOf(pattern[1]);
		
	   double startDate = values.get(0).getValueDate().getTime();   
	      
	   for(DatapointValue val: values){
		   ScriptEngineManager manager = new ScriptEngineManager ();
		   ScriptEngine engine = manager.getEngineByName ("js");
		   String script = function;
		      
		   double expectedVal = val.getValue();
		   double xVal = (val.getValueDate().getTime()-startDate)/1000; //get relative time
		   engine.put("x", xVal);
		   
		    try {
		        Double finalVal = (Double) engine.eval (script);
		        if(finalVal > expectedVal + tolerance || finalVal<expectedVal-tolerance){
		        	return false;
		        }
		        	
		        } catch (ScriptException e) {
		            e.printStackTrace();
		    }
	   }
		return true;
	}
	

}
