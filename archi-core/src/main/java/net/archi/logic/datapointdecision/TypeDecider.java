package net.archi.logic.datapointdecision;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointTypeRepository;
import net.archi.logic.datapoint.DatapointValue;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TypeDecider {
	
	@Resource	DatapointTypeRepository datapointTypeRepository; 
	@Resource	ConstantTypeDecision constantTypeDecision;
	@Resource	IntervalTypeDecision intervalTypeDecision;
	@Resource	FunctionTypeDecision functionTypeDecision;
	
	public DatapointType getDatapointTypeForDatapointVals(List<DatapointValue> datapointVals){
		
		List<DatapointType> datapointTypes = datapointTypeRepository.findAll();
		
		List<DatapointType> matchingTypes = new ArrayList<DatapointType>();
		
		for(DatapointType type : datapointTypes){
			if(StringUtils.isEmpty(type.getPattern())){
				continue;
			}
			if(type.getPatterntype().equals("function")){
				if(functionTypeDecision.isThisTypeValid(type, datapointVals)){
					matchingTypes.add(type);
				}
			} else if(type.getPatterntype().equals("interval")){
				if(intervalTypeDecision.isThisTypeValid(type, datapointVals)){
					matchingTypes.add(type);
				}
			}else if (type.getPatterntype().equals("constant")){
				if(constantTypeDecision.isThisTypeValid(type, datapointVals)){
					matchingTypes.add(type);
				}
			} 
		}
		if(matchingTypes.size() <1 )
			return null;
		
		return matchingTypes.get(0);
	}
}
