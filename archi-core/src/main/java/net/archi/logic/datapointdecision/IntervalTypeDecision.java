package net.archi.logic.datapointdecision;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointValue;
import net.archi.logic.datapoint.DatapointValueRepository;

import org.springframework.stereotype.Component;

@Component
public class IntervalTypeDecision {
	
	@Resource
	DatapointValueRepository datapointValueRepository;
	
	public boolean isThisTypeValid(DatapointType type, List<DatapointValue> datapointVals){
		String[] pattern = type.getPattern().split(";"); //pattern: intervalDuration;IntervalDurationTolerance;maxVal;maxValToleranz;maxValDuration(secs); maxValDurationTolereanz  
		Double interValDur = Double.valueOf(pattern[0]);
		Double interValDurToleranz = Double.valueOf(pattern[1]);
		Double maxVal = Double.valueOf(pattern[2]);
		Double maxValToleranz = Double.valueOf(pattern[3]);
		Double maxValDuration = Double.valueOf(pattern[4]);
		Double maxValDurationToleranz = Double.valueOf(pattern[5]);
		
	
		Date maxValStart = null;
		Date maxValEnd = null;
				
				for(DatapointValue flowVal : datapointVals){
					if(flowVal.getValue() > maxVal + maxValToleranz){ // mayimal wert
						return false;
						// zu grosser wert
					}
					if(flowVal.getValue() > maxVal - maxValToleranz && flowVal.getValue()< maxVal + maxValToleranz){ //viertes kriterium: max val dauer
						//is max Val;
						if(maxValStart == null || maxValStart.getTime() > flowVal.getValueDate().getTime()){
							maxValStart = flowVal.getValueDate();
						}
						if(maxValEnd == null || maxValEnd.getTime() < flowVal.getValueDate().getTime()){
							maxValEnd = flowVal.getValueDate();
						}
					}
					
				}
				long maxValDur = (maxValEnd.getTime() - maxValStart.getTime())/1000; //to seconds
				if(maxValDur > maxValDuration + maxValDurationToleranz ||  maxValDur < maxValDuration - maxValDurationToleranz){
					//max val ist zu lange, starval ist nicht valide
					return false;
				}
				
				//dauer des intervals?
				Date startTime = datapointVals.get(0).getValueDate();
				double sTimeD = startTime.getTime()/1000;
				Date endTime = datapointVals.get(datapointVals.size()-1).getValueDate();
				double eTimeD = endTime.getTime()/1000;
				if(eTimeD -sTimeD > interValDur + interValDurToleranz || eTimeD -sTimeD < interValDur - interValDurToleranz){ //lasts to long?
					return false;
				}
		
		
		return true;
	}
	

	
}
