package net.archi.logic.datapointdecision;

import java.util.List;

import org.springframework.stereotype.Component;

import net.archi.logic.datapoint.DatapointType;
import net.archi.logic.datapoint.DatapointValue;


@Component
public class ConstantTypeDecision {
	
	
	public  boolean isThisTypeValid(DatapointType type, List<DatapointValue> values){
		String pattern = type.getPattern();
		double sum = 0.0;
		for(DatapointValue val: values){
			double doubleVal =  Double.valueOf(val.getValue());
			if(!isNullVal(val)&& !(type.getMaxValue() < doubleVal && patternMatches(doubleVal, pattern))){
				return false;
			}
			sum = sum + doubleVal;
		}
		Double normalval = Double.valueOf(pattern.split(";")[0]);
		Double mxVariance = Double.valueOf(pattern.split(";")[2]);
		Double averageVar = (sum/values.size())- normalval;
		
		if(!(averageVar < mxVariance)){ //average values must be smaller than that
			return false;
		}
		
		return true;
	}
	
	private boolean patternMatches(double value, String pattern){
		String[] patternArray = pattern.split(";");
		double valueOFPattern = Double.valueOf(patternArray[0]);
		double tolerance = Double.valueOf(patternArray[1]);
		
		if(value < (valueOFPattern + tolerance) && value > (valueOFPattern - tolerance)){
			return true;
		}
		
		return false;
	}
	
	private boolean isNullVal(DatapointValue v){
		if(v.getValue() < 100){
			return true;
		}
		return false;
	}
}
