package net.archi.logic;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;

public abstract class GenericBeanWrapper<B extends GenericBean, S extends GenericSmartObject<B>> {

	@Resource
	private ApplicationContext ctx;
	
	protected abstract Class<S> getType();
	
	public S wrap(B bean) {
		if (bean != null) {
			return ctx.getBean(getType(), bean);
		} else {
			return null;
		}
	}
	
	public List<S> wrap(List<B> beans) {
		List<S> list = new ArrayList<S>();
		for(B bean: beans){
			list.add(ctx.getBean(getType(), bean));
		}		
		return list;
	}
	
	public B unwrap(S object) {
		return object.bean();
	}

}
