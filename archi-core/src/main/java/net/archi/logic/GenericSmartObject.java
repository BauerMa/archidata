package net.archi.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public abstract class GenericSmartObject<B extends GenericBean> {
	
	@Autowired
	protected ApplicationContext ctx;
		
	protected B bean;
	
	protected GenericSmartObject(B bean) {
		this.bean = bean;
	}

	protected abstract GenericBeanDao<B> getBeanDao();
	
	B bean() {
		return bean;
	}
	
	public void save() {
		getBeanDao().save(bean);
	}

	public void delete() {
		getBeanDao().delete(bean);
	}
	
	@Override
	public String toString() {
		return bean.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bean == null) ? 0 : bean.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericSmartObject<?> other = (GenericSmartObject<?>) obj;
		if (bean == null) {
			if (other.bean != null)
				return false;
		} else if (!bean.equals(other.bean))
			return false;
		return true;
	}

	public Long getId() {
		return bean.getId();
	}

	public String getName() {
		return bean.getName();
	}

	public void setName(String name) {
		bean.setName(name);
	}
	
}
