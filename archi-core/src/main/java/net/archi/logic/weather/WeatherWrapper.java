package net.archi.logic.weather;

import org.springframework.stereotype.Component;

import net.archi.logic.GenericBeanWrapper;

@Component
public class WeatherWrapper extends GenericBeanWrapper<WeatherBean, Weather> {
	
	@Override
	protected Class<Weather> getType() {
		return Weather.class;
	}
	
}
