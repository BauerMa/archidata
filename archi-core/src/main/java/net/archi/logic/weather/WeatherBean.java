package net.archi.logic.weather;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

@Entity
@Table(name="WEATHER")
public class WeatherBean extends GenericBean {
	
	private String temperature;
	
	private String windSpeed;
	
	private String sun;
	
	private Date dateOfWeather;
	
	private String longitude;
	
	private String latitude;
	
	private String type; //prediction or measured
	

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}

	public String getSun() {
		return sun;
	}

	public void setSun(String sun) {
		this.sun = sun;
	}

	public Date getDateOfWeather() {
		return dateOfWeather;
	}

	public void setDateOfWeather(Date dateOfWeather) {
		this.dateOfWeather = dateOfWeather;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
}
