package net.archi.logic.weather;

import java.util.Calendar;
import java.util.List;

import net.archi.logic.GenericRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WeatherRepository  extends GenericRepository<WeatherBean, Weather>{
	

	private final Logger LOG = LoggerFactory.getLogger(WeatherRepository.class);

	@Autowired
	private WeatherDao weatherDao;

	@Autowired
	private WeatherWrapper weatherWrapper;
	
	public WeatherRepository() {
	}

	@Override
	public List<Weather> findAll() {
		List<WeatherBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public Weather findById(Long id) throws Exception {
		Weather weather = super.findById(id);
		return weather;
	}
	
	public Weather findLatestWeatherPrediction() throws Exception {
		Weather weather = weatherWrapper.wrap(getBeanDao().findLatestWeather("forecast"));
		return weather;
	}
	
	public Weather findAcutalWeather() throws Exception {
		Weather weather = weatherWrapper.wrap(getBeanDao().findLatestWeather("actual"));
		return weather;
	}
	
	public List<Weather> findLast7() {
		Calendar before = Calendar.getInstance();
		before.add(Calendar.DATE, -7);
		
		List<Weather> weathers = weatherWrapper.wrap(getBeanDao().findLatestWeather("actual", 50));
		return weathers;
	}
	
	
	
	@Override
	public Weather newInstance() {
		Weather weather = super.newInstance();
		return weather;
	}
	
	@Override
	protected Class<WeatherBean> getBeanType() {
		return WeatherBean.class;
	}
	
	@Override
	protected WeatherDao getBeanDao() {
		return weatherDao;
	}
	
	@Override
	protected WeatherWrapper getBeanWrapper() {
		return weatherWrapper;
	}
	
	
}
