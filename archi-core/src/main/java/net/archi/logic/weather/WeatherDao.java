package net.archi.logic.weather;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class WeatherDao  extends GenericBeanDao<WeatherBean> {

	@Override
	protected Class<WeatherBean> getType() {
		return WeatherBean.class;
	}

	
	public WeatherBean findLatestWeather(final String type){
		return ht.execute(new HibernateCallback<WeatherBean>() {
			@Override public WeatherBean doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(WeatherBean.class);
				criteria.add(Restrictions.eq("type",type));
				criteria.addOrder(Order.desc("dateOfWeather"));
				criteria.setMaxResults(1);
				
				@SuppressWarnings("unchecked")
				List<WeatherBean> allweather = criteria.list();
				if(allweather != null && allweather.size() >0){
					return allweather.get(0);
				}
				return null;
			}
		});
	}
	
	public List<WeatherBean> findLatestWeather(final String type, final Date from, final Date to){
		
		return ht.execute(new HibernateCallback< List<WeatherBean>>() {
			@SuppressWarnings("unchecked")
			@Override public  List<WeatherBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(WeatherBean.class);
				criteria.add(Restrictions.eq("type",type));
				criteria.addOrder(Order.desc("dateOfWeather"));

				if (to != null)
					criteria.add(Restrictions.le("dateOfWeather", to));
				if (from != null)
					criteria.add(Restrictions.ge("dateOfWeather", from));
				
				return  criteria.list();
			}
		});
		
	}
	
public List<WeatherBean> findLatestWeather(final String type, final int count){
		
		return ht.execute(new HibernateCallback< List<WeatherBean>>() {
			@SuppressWarnings("unchecked")
			@Override public  List<WeatherBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(WeatherBean.class);
				criteria.add(Restrictions.eq("type",type));
				criteria.addOrder(Order.desc("dateOfWeather"));
				criteria.setMaxResults(count);
				return  criteria.list();
			}
		});
		
	}
}
