package net.archi.logic.weather;

import java.util.Date;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Weather extends GenericSmartObject<WeatherBean> { 
	
	@Autowired 
	WeatherDao weatherDao;
	
	Weather(WeatherBean bean) {
		super(bean);
	}
	
	@Override
	protected WeatherDao getBeanDao() {
		return weatherDao;
	}

	public String toString() {
		return bean.toString();
	}
	
	public String getTemperature() {
		return bean.getTemperature();
	}

	public void setTemperature(String temperature) {
		bean.setTemperature(temperature);
	}

	public String getWindSpeed() {
		return bean.getWindSpeed();
	}

	public void setWindSpeed(String windSpeed) {
		bean.setWindSpeed(windSpeed);
	}

	public String getSun() {
		return bean.getSun();
	}

	public void setSun(String sun) {
		bean.setSun(sun);
	}

	public Date getDateOfWeather() {
		return bean.getDateOfWeather();
	}

	public void setDateOfWeather(Date dateOfWeather) {
		bean.setDateOfWeather(dateOfWeather);
	}

	public String getType() {
		return bean.getType();
	}

	public void setType(String type) {
		bean.setType(type);
	}
	
	public String getLongitude() {
		return bean.getLongitude();
	}

	public void setLongitude(String longitude) {
		bean.setLongitude(longitude);
	}

	public String getLatitude() {
		return bean.getLatitude();
	}

	public void setLatitude(String latitude) {
		bean.setLatitude(latitude);
	}
	
}
