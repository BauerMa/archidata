package net.archi.logic.datapoint.consumption;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class DatapointConsumptionWrapper extends GenericBeanWrapper<DatapointConsumptionBean, DatapointConsumption> {

	@Override
	protected Class<DatapointConsumption> getType() {
		return DatapointConsumption.class;
	}
	
}
