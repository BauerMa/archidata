package net.archi.logic.datapoint;

import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointActionRepository extends GenericRepository<DatapointActionBean, DatapointAction> {
		
	@Autowired
	private DatapointActionDao datapointActionDao;

	@Autowired
	private DatapointActionWrapper datapointActionWrapper;
	
	public DatapointActionRepository() {
	}

	@Override
	public List<DatapointAction> findAll() {
		List<DatapointActionBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public DatapointAction findById(Long id) throws Exception {
		DatapointAction datapoint = super.findById(id);
		return datapoint;
	}
	
	public List<DatapointAction> findAllWithoutType(){
		List<DatapointActionBean> beans = getBeanDao().findAllWithoutType();
		return getBeanWrapper().wrap(beans);
	}
	
	public List<DatapointAction> findByType(Long typeId){
		List<DatapointActionBean> beans = getBeanDao().findByType(typeId);
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public DatapointAction newInstance() {
		DatapointAction datapoint = super.newInstance();
		return datapoint;
	}
	
	@Override
	protected Class<DatapointActionBean> getBeanType() {
		return DatapointActionBean.class;
	}
	
	@Override
	protected DatapointActionDao getBeanDao() {
		return datapointActionDao;
	}
	
	@Override
	protected DatapointActionWrapper getBeanWrapper() {
		return datapointActionWrapper;
	}
	
}
