package net.archi.logic.datapoint.consumption;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

@Entity
@Table(name="DATAPOINT_CONSUMPTION")
public class DatapointConsumptionBean extends GenericBean{

	String year;
	
	String consumption;
	
	int persons;
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getConsumption() {
		return consumption;
	}

	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}

	public int getPersons() {
		return persons;
	}

	public void setPersons(int persons) {
		this.persons = persons;
	}
	
}
