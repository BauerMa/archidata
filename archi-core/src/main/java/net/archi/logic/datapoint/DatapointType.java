package net.archi.logic.datapoint;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointType extends GenericSmartObject<DatapointTypeBean>{
	
	@Autowired 
	DatapointTypeDao datapointTypeDao;
	
	DatapointType(DatapointTypeBean bean) {
		super(bean);
	}
	
	@Override
	protected DatapointTypeDao getBeanDao() {
		return datapointTypeDao;
	}

	public String toString() {
		return bean.toString();
	}
	
	public String getPatterntype() {
		return bean.getPatterntype();
	}

	public void setPatterntype(String patterntype) {
		bean.setPatterntype(patterntype);
	}

	public String getPattern() {
		return bean.getPattern();
	}

	public void setPattern(String pattern) {
		bean.setPattern(pattern);
	}
	
	public Double getMinValue() {
		return bean.getMinValue();
	}

	public void setMinValue(Double minValue) {
		bean.setMinValue(minValue);
	}

	public Double getMaxValue() {
		return bean.getMaxValue();
	}

	public void setMaxValue(Double maxValue) {
		bean.setMaxValue(maxValue);;
	}

	public String getUnit() {
		return bean.getUnit();
	}

	public void setUnit(String unit) {
		bean.setUnit(unit);
	}

	
	
}
