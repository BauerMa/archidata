package net.archi.logic.datapoint;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class DatapointTypeWrapper extends GenericBeanWrapper<DatapointTypeBean, DatapointType> {

	@Override
	protected Class<DatapointType> getType() {
		return DatapointType.class;
	}
	
}
