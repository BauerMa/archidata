package net.archi.logic.datapoint;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointRepository extends GenericRepository<DatapointBean, Datapoint> {
		
	@Autowired
	private DatapointDao datapointDao;

	@Autowired
	private DatapointWrapper datapointWrapper;
	
	public DatapointRepository() {
	}

	@Override
	public List<Datapoint> findAll() {
		List<DatapointBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public Datapoint findById(Long id) throws Exception {
		Datapoint datapoint = super.findById(id);
		return datapoint;
	}
	
	
	public List<Datapoint> findByOrignialId(String originalId){
		List<DatapointBean> datapoint = getBeanDao().findByOrignialId(originalId);
		return getBeanWrapper().wrap(datapoint);
	}
	
	@Override
	public Datapoint newInstance() {
		Datapoint datapoint = super.newInstance();
		return datapoint;
	}
	
	@Override
	protected Class<DatapointBean> getBeanType() {
		return DatapointBean.class;
	}
	
	@Override
	protected DatapointDao getBeanDao() {
		return datapointDao;
	}
	
	@Override
	protected DatapointWrapper getBeanWrapper() {
		return datapointWrapper;
	}
	
}
