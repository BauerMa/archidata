package net.archi.logic.datapoint;

import java.sql.SQLException;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class DatapointDao extends GenericBeanDao<DatapointBean> {

	@Override
	protected Class<DatapointBean> getType() {
		return DatapointBean.class;
	}
	
	
	public List<DatapointBean> findByOrignialId(final String originalId){
		return ht.execute(new HibernateCallback<List<DatapointBean>>() {
			@Override public List<DatapointBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointBean.class);
				criteria.add(Restrictions.eq("originalDeviceId",originalId));
				return criteria.list();
			}
		});
	}

	

}
