package net.archi.logic.datapoint;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class DatapointValueDao extends GenericBeanDao<DatapointValueBean> {

	@Override
	protected Class<DatapointValueBean> getType() {
		return DatapointValueBean.class;
	}
	
	public Double findSumOfAll(final Date from, final Date to){
		return ht.execute(new HibernateCallback<Double>() {
			@Override public Double doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointValueBean.class);
				
				if (to != null)
					criteria.add(Restrictions.le("valueDate", to));
				if (from != null)
					criteria.add(Restrictions.ge("valueDate", from));

				criteria.setProjection(Projections.sum("value"));
				
				Double sum = (Double) criteria.uniqueResult();
				return sum;
			}
		});
		
	}
	
	public List<DatapointValueBean> getAllVals(final Long datapointId){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
				@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
					Criteria criteria = session.createCriteria(DatapointValueBean.class);
					criteria.add(Restrictions.eq("datapointBean.id", datapointId));
					criteria.addOrder(Order.asc("valueDate"));
					
					@SuppressWarnings("unchecked")
					List<DatapointValueBean> allpoins = criteria.list();
					return allpoins;
				}
			});
	}
	
	
	public List<DatapointValueBean> getLastValues(final Long datapointId, final int countOfValues, final boolean sortDesc){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
				@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
					Criteria criteria = session.createCriteria(DatapointValueBean.class);
					criteria.add(Restrictions.eq("datapointBean.id", datapointId));
					if(sortDesc){
						criteria.addOrder(Order.desc("valueDate"));
					} else{
						criteria.addOrder(Order.asc("valueDate"));
					}
					
					if(countOfValues > 0){
						criteria.setMaxResults(countOfValues);
					} 
					
					@SuppressWarnings("unchecked")
					List<DatapointValueBean> allpoins = criteria.list();
					return allpoins;
				}
			});
	}
	
	public List<DatapointValueBean> getValuesOfAction(final Long actionId){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
				@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
					Criteria criteria = session.createCriteria(DatapointValueBean.class);
					criteria.add(Restrictions.eq("datapointActionBean.id", actionId));
					
					@SuppressWarnings("unchecked")
					List<DatapointValueBean> allpoins = criteria.list();
					return allpoins;
				}
			});
	}
	
	
	public List<DatapointValueBean> findAllWithoutDatapoint(){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
			@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointValueBean.class);
				criteria.add(Restrictions.isNull("datapointBean"));
				@SuppressWarnings("unchecked")
				List<DatapointValueBean> allpoins = criteria.list();
				return allpoins;
			}
		});
		
	}
	
	public List<DatapointValueBean> findValuesWithVal(final long datapointId, final Double value, final Double tolerance, final Date from, final Date to, final int count){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
			@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointValueBean.class);
				criteria.add(Restrictions.eq("datapointBean.id", datapointId));
				criteria.addOrder(Order.desc("valueDate"));
				criteria.add(Restrictions.ge("value", value-tolerance ));
				criteria.add(Restrictions.le("value", value+tolerance ));
				
				if(count > 0){
					criteria.setMaxResults(count);
				} else {
					criteria.setMaxResults(1000);
				}
				if (to != null)
					criteria.add(Restrictions.le("id.timestamp", to));
				if (from != null)
					criteria.add(Restrictions.ge("id.timestamp", from));
				
				@SuppressWarnings("unchecked")
				List<DatapointValueBean> allpoins = criteria.list();
				return allpoins;
			}
		});
	}
	
	public List<DatapointValueBean> findValueInTimeAreaForType(final Long typeId, final Date from, final Date to){
		return ht.execute(new HibernateCallback<List<DatapointValueBean>>() {
			@Override public List<DatapointValueBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointValueBean.class);
				criteria.add(Restrictions.eq("datapointTypeId", typeId));
				criteria.addOrder(Order.desc("valueDate"));
				
				if (to != null)
					criteria.add(Restrictions.le("valueDate", to));
				if (from != null)
					criteria.add(Restrictions.ge("valueDate", from));
				
				criteria.setMaxResults(1000);
				
				@SuppressWarnings("unchecked")
				List<DatapointValueBean> allpoins = criteria.list();
				return allpoins;
			}
		});
		
	}

}
