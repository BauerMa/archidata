package net.archi.logic.datapoint;

import java.sql.SQLException;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class DatapointActionDao extends GenericBeanDao<DatapointActionBean> {

	@Override
	protected Class<DatapointActionBean> getType() {
		return DatapointActionBean.class;
	}
	
	public List<DatapointActionBean> findAllWithoutType(){
		return ht.execute(new HibernateCallback<List<DatapointActionBean>>() {
			@Override public List<DatapointActionBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointActionBean.class);
				criteria.add(Restrictions.isNull("datapointType"));
				@SuppressWarnings("unchecked")
				List<DatapointActionBean> allpoins = criteria.list();
				return allpoins;
			}
		});
	}
	
	public List<DatapointActionBean> findByType(final Long typeId){
		return ht.execute(new HibernateCallback<List<DatapointActionBean>>() {
			@Override public List<DatapointActionBean> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointActionBean.class);
				criteria.add(Restrictions.eq("datapointType.id",typeId));
				@SuppressWarnings("unchecked")
				List<DatapointActionBean> allpoins = criteria.list();
				return allpoins;
			}
		});
	}
	

}
