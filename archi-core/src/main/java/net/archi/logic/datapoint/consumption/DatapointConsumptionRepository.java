package net.archi.logic.datapoint.consumption;

import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointConsumptionRepository extends GenericRepository<DatapointConsumptionBean, DatapointConsumption> {
		
	@Autowired
	private DatapointConsumptionDao datapointConsumptionDao;

	@Autowired
	private DatapointConsumptionWrapper datapointConsumptionWrapper;
	
	public DatapointConsumptionRepository() {
	}

	@Override
	public List<DatapointConsumption> findAll() {
		List<DatapointConsumptionBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public DatapointConsumption findById(Long id) throws Exception {
		DatapointConsumption datapointConsumption = super.findById(id);
		return datapointConsumption;
	}
	
	
	public DatapointConsumption findByYearAndPersons(String year, int persons) throws Exception {
		DatapointConsumptionBean datapointConsumption = getBeanDao().findByYearAndPersons(year, persons);
		return getBeanWrapper().wrap(datapointConsumption);
	}
	
	
	@Override
	public DatapointConsumption newInstance() {
		DatapointConsumption datapointConsumption = super.newInstance();
		return datapointConsumption;
	}
	
	@Override
	protected Class<DatapointConsumptionBean> getBeanType() {
		return DatapointConsumptionBean.class;
	}
	
	@Override
	protected DatapointConsumptionDao getBeanDao() {
		return datapointConsumptionDao;
	}
	
	@Override
	protected DatapointConsumptionWrapper getBeanWrapper() {
		return datapointConsumptionWrapper;
	}
	
}
