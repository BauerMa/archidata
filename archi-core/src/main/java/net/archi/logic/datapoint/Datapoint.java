package net.archi.logic.datapoint;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Datapoint extends GenericSmartObject<DatapointBean>{
	
	@Autowired 
	DatapointDao datapointDao;
	
	Datapoint(DatapointBean bean) {
		super(bean);
	}
	
	@Override
	protected DatapointDao getBeanDao() {
		return datapointDao;
	}

	public String toString() {
		return bean.toString();
	}
	
	public void setFactor(double factor) {
		bean.setFactor(factor);;
	}

	public String getLabel() {
		return bean.getLabel();
	}

	public void setLabel(String label) {
		bean.setLabel(label);
	}

	public String getUnit() {
		return bean.getUnit();
	}

	public void setUnit(String unit) {
		bean.setUnit(unit);
	}
	
	public String getOriginalId(){
		return bean.getOriginalDeviceId();
	}
	
	public void setOriginalId(String originalDeviceId){
		bean.setOriginalDeviceId(originalDeviceId);
	}

	
	
}
