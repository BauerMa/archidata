package net.archi.logic.datapoint;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointAction extends GenericSmartObject<DatapointActionBean>{
	
	@Autowired 
	DatapointActionDao datapointActionDao;
	
	@Autowired
	DatapointTypeWrapper datapointTypeWrapper;
	
	DatapointAction(DatapointActionBean bean) {
		super(bean);
	}
	
	@Override
	protected DatapointActionDao getBeanDao() {
		return datapointActionDao;
	}

	public String toString() {
		return bean.toString();
	}
	
	public DatapointType getDatapointType() {
		return datapointTypeWrapper.wrap(bean.getDatapointType());
	}

	public void setDatapointType(DatapointType datapointType) {
		bean.setDatapointType(datapointTypeWrapper.unwrap(datapointType));
	}
	
}
