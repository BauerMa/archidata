package net.archi.logic.datapoint;

import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointTypeRepository extends GenericRepository<DatapointTypeBean, DatapointType> {
	
	@Autowired
	private DatapointTypeDao datapointTypeDao;

	@Autowired
	private DatapointTypeWrapper datapointTypeWrapper;
	
	public DatapointTypeRepository() {
	}

	@Override
	public List<DatapointType> findAll() {
		List<DatapointTypeBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public DatapointType findById(Long id) throws Exception {
		DatapointType datapointType = super.findById(id);
		return datapointType;
	}
	
	public DatapointType findByPatternType(String patternType) throws Exception {
		DatapointTypeBean datapointType = getBeanDao().findByPatternType(patternType);
		return getBeanWrapper().wrap(datapointType);
	}
	
	public DatapointType findByName(String name){
		DatapointTypeBean datapointType = getBeanDao().findByName(name);
		return getBeanWrapper().wrap(datapointType);
	}
	
	@Override
	public DatapointType newInstance() {
		DatapointType datapointType = super.newInstance();
		return datapointType;
	}
	
	@Override
	protected Class<DatapointTypeBean> getBeanType() {
		return DatapointTypeBean.class;
	}
	
	@Override
	protected DatapointTypeDao getBeanDao() {
		return datapointTypeDao;
	}
	
	@Override
	protected DatapointTypeWrapper getBeanWrapper() {
		return datapointTypeWrapper;
	}
	
}
