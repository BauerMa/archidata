package net.archi.logic.datapoint.consumption;

import java.sql.SQLException;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class DatapointConsumptionDao extends GenericBeanDao<DatapointConsumptionBean> {

	@Override
	protected Class<DatapointConsumptionBean> getType() {
		return DatapointConsumptionBean.class;
	}
	
	public  DatapointConsumptionBean findByYearAndPersons(final String year,final int persons){
		return ht.execute(new HibernateCallback<DatapointConsumptionBean>() {
			@Override public DatapointConsumptionBean doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointConsumptionBean.class);
				criteria.add(Restrictions.eq("year",year));
				criteria.add(Restrictions.eq("persons",persons));
						
				@SuppressWarnings("unchecked")
				List<DatapointConsumptionBean> all = criteria.list();
				if(all != null && all.size() >0){
					return all.get(0);
				}
				return null;
			}
		});
	}
	
}
