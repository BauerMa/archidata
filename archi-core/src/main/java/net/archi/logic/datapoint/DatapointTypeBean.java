package net.archi.logic.datapoint;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

@Entity
@Table(name="DATAPOINT_TYPES")
public class DatapointTypeBean extends GenericBean{
	
	Double minValue;
	
	Double maxValue;
	
	String pattern;
	
	String unit;
	
	String patterntype;
	

	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	public Double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPatterntype() {
		return patterntype;
	}

	public void setPatterntype(String patterntype) {
		this.patterntype = patterntype;
	}
	
}
