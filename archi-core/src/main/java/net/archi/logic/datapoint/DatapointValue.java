package net.archi.logic.datapoint;

import java.util.Date;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointValue extends GenericSmartObject<DatapointValueBean>{
	
	@Autowired 
	DatapointValueDao datapointValueDao;
	
	@Autowired
	DatapointWrapper datapointWrapper;

	@Autowired
	DatapointActionWrapper datapointActionWrapper;
	
	
	DatapointValue(DatapointValueBean bean) {
		super(bean);
	}
	
	@Override
	protected DatapointValueDao getBeanDao() {
		return datapointValueDao;
	}

	public String toString() {
		return bean.toString();
	}
	public Double getValue() {
		return bean.getValue();
	}
	public void setValue(Double value) {
		bean.setValue(value);;
	}
	public Date getValueDate() {
		return bean.getValueDate();
	}
	public void setValueDate(Date value) {
		bean.setValueDate(value);;
	}
	public String getOriginalId(){
		return bean.getOriginalDevideId();
	}
	public void setOriginalId(String originalDevideId){
		bean.setOriginalDevideId(originalDevideId);
	}
	public Datapoint getDatapointBean() {
		return datapointWrapper.wrap(bean.getDatapointBean());
	}
	public void setDatapointBean(Datapoint datapoint) {
		if(datapoint != null){
			bean.setDatapointBean(datapointWrapper.unwrap(datapoint));
		} else{
			bean.setDatapointBean(null);
		}
	}
	public DatapointAction getDatapointActionBean() {
		return datapointActionWrapper.wrap(bean.getDatapointActionBean());
	}
	public void setDatapointActionBean(DatapointAction datapointActionBean) {
		bean.setDatapointActionBean(datapointActionWrapper.unwrap(datapointActionBean));
	}
	public Long getDatapointTypeId(){
		return bean.getDatapointTypeId();
	}
	public void setDatapointTypeId(Long type){
		bean.setDatapointTypeId(type);
	}
}
