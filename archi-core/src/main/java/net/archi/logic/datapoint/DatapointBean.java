package net.archi.logic.datapoint;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

@Entity
@Table(name="DATAPOINT_BEANS")
public class DatapointBean extends GenericBean{
	
	
	@Column(length=256)
	private String label;
	
	@Column(length=16)
	private String unit;
			
	private double factor = 1.0;
	
	private String originalDeviceId;


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getOriginalDeviceId() {
		return originalDeviceId;
	}

	public void setOriginalDeviceId(String originalDeviceId) {
		this.originalDeviceId = originalDeviceId;
	}
	
}
