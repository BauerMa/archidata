package net.archi.logic.datapoint;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.archi.logic.GenericBean;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="DATAPOINT_VALUE_BEANS")
public class DatapointValueBean extends GenericBean{
	
	private Double value;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date valueDate;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.DETACH, optional=true)
	@Fetch(FetchMode.SELECT)
	@ForeignKey(name="FK_DATAPOINTVALUE_DATAPOINTID")
	private DatapointBean datapointBean;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH, optional=true)
	@Fetch(FetchMode.SELECT)
	@ForeignKey(name="FK_DATAPOINTVALUE_DATAPOINTACTIONID")
	private DatapointActionBean datapointActionBean;
	
	private Long datapointTypeId;
	
	private String originalDevideId;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public DatapointBean getDatapointBean() {
		return datapointBean;
	}
	public void setDatapointBean(DatapointBean datapointBean) {
		this.datapointBean = datapointBean;
	}
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	public String getOriginalDevideId() {
		return originalDevideId;
	}
	public void setOriginalDevideId(String originalDevideId) {
		this.originalDevideId = originalDevideId;
	}
	public DatapointActionBean getDatapointActionBean() {
		return datapointActionBean;
	}
	public void setDatapointActionBean(DatapointActionBean datapointActionBean) {
		this.datapointActionBean = datapointActionBean;
	}

	public Long getDatapointTypeId() {
		return datapointTypeId;
	}

	public void setDatapointTypeId(Long datapointTypeId) {
		this.datapointTypeId = datapointTypeId;
	}
}
