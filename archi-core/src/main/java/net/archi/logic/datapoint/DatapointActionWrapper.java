package net.archi.logic.datapoint;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class DatapointActionWrapper extends GenericBeanWrapper<DatapointActionBean, DatapointAction> {

	@Override
	protected Class<DatapointAction> getType() {
		return DatapointAction.class;
	}
	
}
