package net.archi.logic.datapoint;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="DATAPOINT_ACTION_BEANS")
public class DatapointActionBean extends GenericBean{
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH, optional=true)
	@Fetch(FetchMode.SELECT)
	@ForeignKey(name="FK_DATAPOINT_DATPOINTTYPEID")
	private DatapointTypeBean datapointType;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	public DatapointTypeBean getDatapointType() {
		return datapointType;
	}

	public void setDatapointType(DatapointTypeBean datapointType) {
		this.datapointType = datapointType;
	}

}
