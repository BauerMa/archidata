package net.archi.logic.datapoint;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class DatapointValueWrapper extends GenericBeanWrapper<DatapointValueBean, DatapointValue> {

	@Override
	protected Class<DatapointValue> getType() {
		return DatapointValue.class;
	}
	
}
