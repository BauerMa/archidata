package net.archi.logic.datapoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.archi.logic.GenericRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointValueRepository extends GenericRepository<DatapointValueBean, DatapointValue> {
	
	private final Logger LOG = LoggerFactory.getLogger(DatapointValueRepository.class);

	@Autowired
	private DatapointValueDao datapointValueDao;

	@Autowired
	private DatapointValueWrapper datapointValueWrapper;
	
	public DatapointValueRepository() {
	}

	@Override
	public List<DatapointValue> findAll() {
		List<DatapointValueBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	public List<DatapointValue> findAllByAction(Long actionId) {
		List<DatapointValueBean> beans = getBeanDao().getValuesOfAction(actionId);
		return getBeanWrapper().wrap(beans);
	}
	
	public Double findSumOfAll(Date from, Date to) {
		Double ergebnis = getBeanDao().findSumOfAll(from, to);
		if(ergebnis == null || ergebnis < 1){
			ergebnis = 0D;
			LOG.debug("leerer wert fuer " + from.toLocaleString() + " to " + to.toLocaleString());
		}
		return ergebnis;
	}
	
	
	public List<DatapointValue> findAllByDatapoint(Long datapoint) {
		List<DatapointValueBean> beans = getBeanDao().getAllVals(datapoint);
		return getBeanWrapper().wrap(beans);
	}
	
	public List<DatapointValue> findAllWithoutDatapoint(){
		List<DatapointValueBean> beans = getBeanDao().findAllWithoutDatapoint();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public DatapointValue findById(Long id) throws Exception {
		DatapointValue datapointValue = super.findById(id);
		return datapointValue;
	}
	
	public List<DatapointValue> findValuesWithVal(Long datapointId, Double value, Double tolerance, Date from, Date to, int count) throws Exception {
		 List<DatapointValueBean> datapointValues = getBeanDao().findValuesWithVal(datapointId, value, tolerance, from, to, count);
		return  getBeanWrapper().wrap(datapointValues);
	}
	
	public List<DatapointValue> findValueInTimeAreaForType(Long typeId,Date from, Date to) throws Exception {
		 List<DatapointValueBean> datapointValues = getBeanDao().findValueInTimeAreaForType(typeId, from, to);
		return  getBeanWrapper().wrap(datapointValues);
	}
	
	public List<DatapointValue> getLastValues(Datapoint datapoint, int countOfValues){
		List<DatapointValueBean> beans = getBeanDao().getLastValues(datapoint.getId(), countOfValues, true);
		return getBeanWrapper().wrap(beans);
	}

	@Override
	public DatapointValue newInstance() {
		DatapointValue datapointValue = super.newInstance();
		return datapointValue;
	}
	
	@Override
	protected Class<DatapointValueBean> getBeanType() {
		return DatapointValueBean.class;
	}
	
	@Override
	protected DatapointValueDao getBeanDao() {
		return datapointValueDao;
	}
	
	@Override
	protected DatapointValueWrapper getBeanWrapper() {
		return datapointValueWrapper;
	}
	
}
