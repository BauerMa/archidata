package net.archi.logic.datapoint;

import java.sql.SQLException;
import java.util.List;

import net.archi.logic.GenericBeanDao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class DatapointTypeDao extends GenericBeanDao<DatapointTypeBean> {

	@Override
	protected Class<DatapointTypeBean> getType() {
		return DatapointTypeBean.class;
	}
	
	
	public DatapointTypeBean findByPatternType(final String patternType){
		return ht.execute(new HibernateCallback<DatapointTypeBean>() {
			@Override public DatapointTypeBean doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointTypeBean.class);
				criteria.add(Restrictions.eq("patternType",patternType));
				
				@SuppressWarnings("unchecked")
				List<DatapointTypeBean> allptype = criteria.list();
				if(allptype != null && allptype.size() > 0){
					return allptype.get(0);
				} else{
					return null;
				}
			}
		});
		
	}
	
	public DatapointTypeBean findByName(final String name){
		return ht.execute(new HibernateCallback<DatapointTypeBean>() {
			@Override public DatapointTypeBean doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(DatapointTypeBean.class);
				criteria.add(Restrictions.eq("name",name));
				
				@SuppressWarnings("unchecked")
				List<DatapointTypeBean> allptype = criteria.list();
				if(allptype != null && allptype.size() > 0){
					return allptype.get(0);
				} else{
					return null;
				}
			}
		});
	}

}
