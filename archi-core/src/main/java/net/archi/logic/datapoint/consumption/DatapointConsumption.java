package net.archi.logic.datapoint.consumption;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DatapointConsumption extends GenericSmartObject<DatapointConsumptionBean>{
	
	@Autowired 
	DatapointConsumptionDao datapointConsumptionDao;
	
	DatapointConsumption(DatapointConsumptionBean bean) {
		super(bean);
	}
	
	@Override
	protected DatapointConsumptionDao getBeanDao() {
		return datapointConsumptionDao;
	}

	public String toString() {
		return bean.toString();
	}
	
	public String getYear() {
		return bean.getYear();
	}

	public void setYear(String year) {
		bean.setYear(year);
	}

	public String getConsumption() {
		return bean.getConsumption();
	}

	public void setConsumption(String consumption) {
		bean.setConsumption(consumption);
	}

	public int getPersons() {
		return bean.getPersons();
	}

	public void setPersons(int persons) {
		bean.setPersons(persons);
	}
	

	
}
