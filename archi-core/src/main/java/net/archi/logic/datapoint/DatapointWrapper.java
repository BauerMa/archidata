package net.archi.logic.datapoint;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class DatapointWrapper extends GenericBeanWrapper<DatapointBean, Datapoint> {

	@Override
	protected Class<Datapoint> getType() {
		return Datapoint.class;
	}
	
}
