package net.archi.logic;

import java.util.List;

public abstract class GenericRepository<B extends GenericBean, S extends GenericSmartObject<B>> {

	protected abstract GenericBeanDao<B> getBeanDao();
	
	protected abstract GenericBeanWrapper<B, S> getBeanWrapper();
	
	protected abstract Class<B> getBeanType(); 
	
	public List<S> findAll() {
		return getBeanWrapper().wrap(getBeanDao().findAll());
	}
	
	public S findById(Long id) throws Exception {
		B bean = getBeanDao().findById(id);
		if (bean == null) {
			throw new Exception("object of type " + getBeanType().getSimpleName() + " not found with id " + id);
		}
		return getBeanWrapper().wrap(bean);
	}
	
	public S newInstance() {
		try {
			B bean = getBeanType().newInstance();
			S object = getBeanWrapper().wrap(bean);
			return object;
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
		
}
