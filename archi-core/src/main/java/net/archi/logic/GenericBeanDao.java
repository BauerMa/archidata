package net.archi.logic;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.orm.hibernate3.HibernateTemplate;

public abstract class GenericBeanDao<T extends GenericBean> {

	@Resource
	protected HibernateTemplate ht;
	
	protected abstract Class<T> getType();
	
	public T findById(Long id) {
		return ht.get(getType(), id);
	}
	
	public List<T> findAll() {
		return ht.loadAll(getType());
	}
	
	public void save(T bean) {
		ht.saveOrUpdate(bean);
	}
	
	public void delete(T bean) {
		ht.delete(bean);
	}
	
}
