package net.archi.logic.user;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class GlobalUserRepository extends GenericRepository<UserBean, User> {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired 
	private UserWrapper userWrapper;
	
	public User findByLogin(String login) throws Exception {
		UserBean bean = getBeanDao().findByLogin(login);
		if (bean == null) {
			throw new Exception("user not found with login: " + login);
		}
		return getBeanWrapper().wrap(bean);
	}
	
	public User findByEmail(String email) throws Exception {
		UserBean bean = getBeanDao().findByEmail(email);
		if (bean == null) {
			throw new Exception("user not found with email: " + email);
		}
		return getBeanWrapper().wrap(bean);
	}

	
	@Override
	protected Class<UserBean> getBeanType() {
		return UserBean.class;
	}	
	
	@Override
	protected UserDao getBeanDao() {
		return userDao;
	}

	@Override
	protected UserWrapper getBeanWrapper() {
		return userWrapper;
	}

}
