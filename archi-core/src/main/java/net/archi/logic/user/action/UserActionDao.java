package net.archi.logic.user.action;

import net.archi.logic.GenericBeanDao;

import org.springframework.stereotype.Component;

@Component
public class UserActionDao extends GenericBeanDao<UserActionBean> {

	@Override
	protected Class<UserActionBean> getType() {
		return UserActionBean.class;
	}
	
	
}
