package net.archi.logic.user;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class UserWrapper extends GenericBeanWrapper<UserBean, User> {

	@Override
	protected Class<User> getType() {
		return User.class;
	}
	
}
