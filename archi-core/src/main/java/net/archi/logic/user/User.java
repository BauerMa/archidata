package net.archi.logic.user;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class User extends GenericSmartObject<UserBean> {
	
	@Autowired
	UserDao userDao;
	
	User(UserBean bean) {
		super(bean);
	}
	
	@Override
	protected UserDao getBeanDao() {
		return userDao;
	}

	
	@Override
	public void save() {
		super.save();
	}
	

	public String getLastName() {
		return bean.getLastName();
	}

	public void setLastName(String lastName) {
		bean.setLastName(lastName);
	}
	
}
