package net.archi.logic.user.action;

import net.archi.logic.GenericSmartObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserAction extends GenericSmartObject<UserActionBean> {
	
	@Autowired
	UserActionDao userActionDao;
	
	UserAction(UserActionBean bean) {
		super(bean);
	}
	
	@Override
	protected UserActionDao getBeanDao() {
		return userActionDao;
	}

	
	@Override
	public void save() {
		super.save();
	}
	
	
	
}
