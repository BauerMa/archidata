package net.archi.logic.user;

import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserRepository extends GenericRepository<UserBean, User> {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired 
	private UserWrapper userWrapper;
	
	public UserRepository() {
	}
	
	@Override
	public List<User> findAll() {
		List<UserBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public User findById(Long id) throws Exception {
		User user = super.findById(id);
		return user;
	}
	
	@Override
	public User newInstance() {
		User user = super.newInstance();
		return user;
	}
	
	@Override
	protected Class<UserBean> getBeanType() {
		return UserBean.class;
	}	
	
	@Override
	protected UserDao getBeanDao() {
		return userDao;
	}

	@Override
	protected UserWrapper getBeanWrapper() {
		return userWrapper;
	}
	
}
