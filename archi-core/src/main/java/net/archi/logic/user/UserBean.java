package net.archi.logic.user;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.archi.logic.GenericBean;

@Entity
@Table(name="USERS")
public class UserBean extends GenericBean {

	private String lastName;
	
	public UserBean() {
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
