package net.archi.logic.user;

import java.sql.SQLException;

import net.archi.logic.GenericBeanDao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

@Component
public class UserDao extends GenericBeanDao<UserBean> {

	@Override
	protected Class<UserBean> getType() {
		return UserBean.class;
	}
	
	public UserBean findByLogin(final String login) {
		return ht.execute(new HibernateCallback<UserBean>() {
			@Override public UserBean doInHibernate(Session session) throws HibernateException, SQLException {
				return (UserBean)session.createCriteria(UserBean.class)
						.add(Restrictions.eq("login", login).ignoreCase())
						.uniqueResult();
			}
		});
	}
	
	public UserBean findByEmail(final String email) {
		return ht.execute(new HibernateCallback<UserBean>() {
			@Override public UserBean doInHibernate(Session session) throws HibernateException, SQLException {
				return (UserBean)session.createCriteria(UserBean.class)
						.add(Restrictions.eq("email", email).ignoreCase())
						.uniqueResult();
			}
		});
	}
}
