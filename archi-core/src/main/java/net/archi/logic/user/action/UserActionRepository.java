package net.archi.logic.user.action;

import java.util.List;

import net.archi.logic.GenericRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserActionRepository extends GenericRepository<UserActionBean, UserAction> {
	
	@Autowired
	private UserActionDao userActionDao;
	
	@Autowired 
	private UserActionWrapper userActionWrapper;
	
	public UserActionRepository() {
	}
	
	@Override
	public List<UserAction> findAll() {
		List<UserActionBean> beans = getBeanDao().findAll();
		return getBeanWrapper().wrap(beans);
	}
	
	@Override
	public UserAction findById(Long id) throws Exception {
		UserAction userAction = super.findById(id);
		return userAction;
	}
	
	@Override
	public UserAction newInstance() {
		UserAction userAction = super.newInstance();
		return userAction;
	}
	
	@Override
	protected Class<UserActionBean> getBeanType() {
		return UserActionBean.class;
	}	
	
	@Override
	protected UserActionDao getBeanDao() {
		return userActionDao;
	}

	@Override
	protected UserActionWrapper getBeanWrapper() {
		return userActionWrapper;
	}
	
}
