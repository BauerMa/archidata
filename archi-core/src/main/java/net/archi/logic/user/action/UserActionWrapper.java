package net.archi.logic.user.action;

import net.archi.logic.GenericBeanWrapper;

import org.springframework.stereotype.Component;

@Component
public class UserActionWrapper extends GenericBeanWrapper<UserActionBean, UserAction> {

	@Override
	protected Class<UserAction> getType() {
		return UserAction.class;
	}
	
}
