package net.archi.logic.externalDB;

import java.util.Date;

public class ExternalDBValueBean {
	
	private Double value;
	
	private Date timestamp;
	
	private String tableSource;
	
	public ExternalDBValueBean(){}
	

	public ExternalDBValueBean(Double value, Date timestamp, String tableSource) {
		super();
		this.value = value;
		this.timestamp = timestamp;
		this.tableSource = tableSource;
	}


	public String getTableSource() {
		return tableSource;
	}

	public void setTableSource(String tableSource) {
		this.tableSource = tableSource;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
