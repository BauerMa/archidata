package net.archi.logic.externalDB;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;


@Component
public class ValueDBConnector {
	
	private final Logger LOG = Logger.getLogger(ValueDBConnector.class);
	
	
	public HashMap< String, String> generateTableNames(){
		
		HashMap< String, String> tableNames = new HashMap<>();
		Connection con = null;

		try {
			con = DriverManager.getConnection(
							"jdbc:mysql://mysql.idp.ai.ar.tum.de/openhab",
							"mbauer", "idp2015");
			
			Statement stmt = con.createStatement();
			String query = "select * from Items;";
			ResultSet rs = stmt.executeQuery( query );
			
			while ( rs.next() ){
				tableNames.put("Item" + rs.getString(1), rs.getString(2));
			}
			 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		
		
		
		return tableNames;
	}


	public List<ExternalDBValueBean> generateValueSQLQuery(Date from, Date to) throws SQLException{
		List<ExternalDBValueBean> valuesFromDB = new ArrayList<ExternalDBValueBean>();
		try{
		  Class.forName( "com.mysql.jdbc.Driver" );
		  DriverManager.setLogWriter( new PrintWriter( System.out ) );
		}catch (ClassNotFoundException e)		{
			LOG.error("driver can not be found");
			return null;
		}
		Connection con = null;
		
		List<String> tables = new ArrayList<String>();

		try {
			con = DriverManager.getConnection(
							"jdbc:mysql://mysql.idp.ai.ar.tum.de/openhab",
							"mbauer", "idp2015");
			
			Statement stmt = con.createStatement();
			String query = "show tables;";
			ResultSet rs = stmt.executeQuery( query );
			
			while ( rs.next() ){
				tables.add(rs.getString(1));
			}
			
			for(String table: tables){
				if(!table.equals("Items")){
					valuesFromDB.addAll(getValuesFromSingleTable(table, from, to, con));
				}
			}

			 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		
		
		
		return valuesFromDB;
	}
	
	
	
	private List<ExternalDBValueBean> getValuesFromSingleTable(String tableName, Date from, Date to, Connection con) throws SQLException{
		List<ExternalDBValueBean> valuesFromThisTable = new ArrayList<ExternalDBValueBean>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Statement stmt = con.createStatement(); //STR_TO_DATE('2009-06-29 04:00:44', '%Y-%m-%d %H:%i:%s');
		String query = "select * from " + tableName;
			
			
		if(from != null){
			query +=  " where Time BETWEEN STR_TO_DATE('" + sdf.format(from) +"', '%Y-%m-%d %H:%i:%s') AND "+
					"STR_TO_DATE('" + sdf.format(to) + "', '%Y-%m-%d %H:%i:%s');";
		}else{
			query += ";";
		}
		
		ResultSet rs = stmt.executeQuery( query );
		
		
		while ( rs.next() ){
			ExternalDBValueBean actVal = new ExternalDBValueBean();
			actVal.setTableSource(tableName);
			actVal.setTimestamp( rs.getTimestamp(1));
			if(rs.getString(2).equals("OPEN") || rs.getString(2).equals("ON")){
				actVal.setValue(new Double(1));
			} else if(rs.getString(2).equals("CLOSED")|| rs.getString(2).equals("OFF")){
				actVal.setValue(new Double(0));
			} else{
				actVal.setValue(rs.getDouble(2));
			}
			
			valuesFromThisTable.add(actVal);
		}
		
		return valuesFromThisTable;
		
	}

}
